import express from "express";
import bodyParser, { json } from "body-parser";
import * as mockedData from "../resources/MockedListData.json";
import { strict } from "assert";
import { callbackify } from "util";
import { any } from "bluebird";

var app = express();

const port = 8080;

// parse application/x-www-form-urlencoded from body
app.use(bodyParser.json());

//parse application.json from body
app.use(bodyParser.urlencoded({ extended: true }));


app.get("/", (request, response) => {
	response.send("Hello world");
});

app.get("/greetings", (req, res) => {
	res.send(`Welcome John Doe!`);
});

app.get("/greetings/:name", (req, res) => {
	res.send(`Welcome ${req.params.name}!`);
});

app.get("/lists", (req, res) => {
	res.send(mockedData.lists);
});

app.get("/list/:id", (req, res) => {
	let idFromRequest = req.params.id;
	let requestList = mockedData.lists.find(el => el.id === idFromRequest);
	if (requestList) {
		res.send(requestList);
	} else {
		res.send("No list found.");
	}
});

// 1 param "message" read from body
app.post("/toConsole", (req, res) => {
	console.log("mergi???");
	if (req.body && req.body.message) {
		console.log(req.body.message);
		res.send("Message was posted to server.");
	} else {
		res.send("No message received. Nothing was posted on sever.");
	}
});

// returning title and body
app.get("/list/:listId/:elementId",(req,res)=>{
	let listIdFromRequest = req.params.listId;
	let elementIdFromRequest = req.params.elementId;
	
	let requestList:any = mockedData.lists.find(el => el.id === listIdFromRequest);
	let dictList = requestList['listElements'];
	let answear;

	res.setHeader('Content-Type', 'text/html');

	for (let dict of dictList){
		if (dict['id']===elementIdFromRequest){
			answear = dict;
			break
		} 
	}
	if(answear){
		res.write( `List id inserted : ${listIdFromRequest}
					<br>Element id inserted : ${elementIdFromRequest}
					<br>Response : ${JSON.stringify(answear)}
					<br>Success : ${true}
					<br>Message : operation ended with success`);
	}
	else{
		res.write(`Success : ${false}
				<br>Message : non existent id`);
	}
	res.end();

});

app.post("/addListElement",(req,res) => {

	`JSON format to be sent 
	{
		"list_id":"<value>",
		"data":{
				"id":"<value>",
				"title":"<value>>",
				"body":"<value>"
		}
	}
	`

	let fs = require("fs");
	const mockedData = require("../resources/MockedListData.json");//reading json file
	let dict = req.body;
	let list_id:any;

	if(dict["list_id"]){
		list_id = dict["list_id"];
	}else {
		console.log({"success":false,"message":"'list_id':<value> missing from JSON"});
		return res.send({"success":false,"message":"'list_id':<value> missing from JSON"});
	}
	let newData:any;

	if(dict["data"]){
		newData = dict["data"];//the new dictionary from post to be created comes here
	}else{
		console.log({"success":false,"message":"'body':{'id':<value>,'title':<value>,'body':<value>} missing from JSON"});
		return res.send({"success":false,"message":"'body':{'id':<value>,'title':<value>,'body':<value>} missing from JSON"});
	}

	let array = mockedData["lists"];
	let targetDict;

	for (let dict of array){
		if (dict['id']===list_id){//searching the dictionary in which we want to create
			targetDict = dict;//stocking the current dict
			break
		}
	}
	
	let listElements = targetDict["listElements"];
	let listElementsUpdate = listElements.push(newData);
	const newMockedList = Object.assign({},listElementsUpdate,mockedData);//to update the dictionary with the new values
	
	fs.writeFile("./resources/MockedListData.json", JSON.stringify(newMockedList,null,4),(err:any) => { 
		if (err) throw err;
		else{
			console.log({	"list_id_received":`${list_id}`,
							"data_received":`${JSON.stringify(newData)}`,
							"success":true,
							"message":`operation ended with success, new element ${JSON.stringify(newData)} has been created in the list with the id: ${list_id}.`});
			return res.send({"success":true,"message":`operation ended with success, new element ${JSON.stringify(newData)} has been created in the list with the id: ${list_id}.`});
		}; 
	}) 
});

app.delete("/deleteListElement",(req,res) =>{

	`JSON format to be sent
	{
		"list_id":"<value>",
		"element_id":"<value>"
	}
	`

	let fs = require("fs");
	const mockedData = require("../resources/MockedListData.json");//reading json file

	let dict = req.body;
	let list_id:any;
	let element_id:any;

	if(dict["list_id"]){
		list_id = dict["list_id"];
	}else {
		console.log({"success":false,"message":"'list_id':<value> missing from JSON"});
		return res.send({"success":false,"message":"'list_id':<value> missing from JSON"});
	}

	if(dict["element_id"]){
		element_id = dict["element_id"];
	}else {
		console.log({"success":false,"message":"'element_id':<value> missing from JSON"});
		return res.send({"success":false,"message":"'element_id':<value> missing from JSON"});
	}
	let array = mockedData["lists"];
	let listElmDictionaries:any;

	for (let dict of array){
		if(dict["id"]===list_id){
			listElmDictionaries=dict["listElements"];
			break
		}
	}
	let index = listElmDictionaries.map(function(o: { id: any; }) { return o.id; }).indexOf(element_id);
	// console.log(`target element index in listElements: ${index}`);
	let deletedElem = listElmDictionaries.splice(index, 1);
	// console.log(`remaining elements in list after splice: ${JSON.stringify(listElmDictionaries)}`);
	mockedData.lists.find((el: { id: any; listElements: any; }) => el.id === list_id ? el.listElements:listElmDictionaries);

	fs.writeFile("./resources/MockedListData.json", JSON.stringify(mockedData,null,4),(err:any) => { 
		if (err) throw err;
		else{
			console.log({	"list_id_received":`${list_id}`,
							"element_id":`${element_id}`,
							"success":true,
							"message":`operation ended with success, element ${JSON.stringify(deletedElem)} has been deleted from the list with the id : ${list_id}`});
			return res.send({"success":true,"message":`operation ended with success, element ${JSON.stringify(deletedElem)} has been deleted from the list with the id : ${list_id}`});
		}; 
	}) 
});

app.listen(port, () => {
	console.log(`Server started at http://localhost:${port}!`);
});
